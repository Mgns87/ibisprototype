## Ontov Visualization

## Technology stack
- Express
- Angular
	- With Bootstrap and Angular-Bootstrap
- Node
- [D3](http://d3js.org/)
- [QueryEngine](https://github.com/bevry/query-engine)

## Get it!
Clone, and then:

```sh
$ npm install
```

Build and run (Build required first time*):
```sh
$ gulp
```

Run without build step and file watching:
```sh
$ npm start
```

Go to http://localhost:3000/ in your browser

