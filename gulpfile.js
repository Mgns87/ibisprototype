var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var nodemon = require('gulp-nodemon');


gulp.task('js', function() {
    return gulp.src(['src/javascript/index.js','src/javascript/services/**/*.js', 'src/javascript/directives/**/*.js'])
    //.pipe(sourcemaps.init())
    .pipe(concat('ontov.js'))
    //.pipe(uglify())
    //.pipe(sourcemaps.write())
    .pipe(gulp.dest('public/ontov/js'));
});

gulp.task('css', function() {
    return gulp.src(['src/stylesheets/**/*.css'])
    .pipe(gulp.dest('public/ontov/css'));
});

gulp.task('move', function(){
    gulp.src(['src/template/**/*']).pipe(gulp.dest('public/ontov/template')); //HTML Directive Templates
    gulp.src(['src/javascript/vendor/**/*.js']).pipe(gulp.dest('public/ontov/js/vendor'));
    gulp.src(['src/javascript/data/**/*.json']).pipe(gulp.dest('public/ontov/js/data'));
});


gulp.task('default',['js', 'css','move'], function() {
    nodemon({
        script: 'app.js',
        ignore: ["public/*"],
        ext: 'js html css',
        tasks: ['js','css', 'move']
    });
});
