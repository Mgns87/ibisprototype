exports.index = function(req, res) {
  res.render('index.ejs');
};

exports.partials = function(req, res) {
  res.render('partials/' + req.params.name);
};