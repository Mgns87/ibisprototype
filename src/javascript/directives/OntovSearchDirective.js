//Searchbar
(function() {
    'use strict';
    angular.module('ontov')
        .directive('visualSearch', ['$rootScope', '$window', 'vsService', 'dataModel', function($rootScope, $window, vsService, dataModel) {
            return {
                restrict: 'E',
                scope: {},
                templateUrl: 'template/searchbar.html',
                link: function(scope, element, attrs) {
                    vsService.get()
                        .then(function(VS) {
                            var visualSearch = VS.init({
                                container: $('.visual_search'),
                                query: '',
                                callbacks: {
                                    search: function(searchString, searchCollection) {
                                        searchString = "";
                                        searchCollection.forEach(function(pill) {
                                            var category = pill.get("category"),
                                                value = pill.get("value");
                                            if (category != "text") {
                                                searchString += " " + category + ":\"" + value + "\""+",";
                                            } else {
                                                searchString += " " + value +",";
                                            }
                                        });
                                        //Remove duplicates in search string
                                        //searchString = _.uniq(searchString.split(" ")).join(" ");
                                        dataModel.search(searchString);

                                    },
                                    facetMatches: function(callback) {
                                        // These are the facets that will be autocompleted in an empty input.
                                        var pills = dataModel.getPills();
                                        var pillNames = _.keys(pills);
                                        callback(pillNames);
                                    },
                                    valueMatches: function(facet, searchTerm, callback) {
                                        // These are the values that match specific categories, autocompleted
                                        // in a category's input field.  searchTerm can be used to filter the
                                        // list on the server-side, prior to providing a list to the widget.
                                        callback(dataModel.getFacetMatches(facet));
                                    }
                                }
                            });
                        });
                }
            }
        }]);

})();