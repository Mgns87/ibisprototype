//Graph Visualization
(function(){
    'use strict';
    angular.module('ontov')
        .directive('ontovViz', ['d3Service', 'qeService', 'dataModel', function(d3Service, qeService, dataModel) {
            return {
                restrict: 'E',
                scope: {
                    width: '@width',
                    height: '@height'
                },
                templateUrl: 'template/visualization.html',
                link: function(scope, element, attrs) {

                    //Init Config
                    var _vm = {}, _d3 = {};
                    var svg = element.find('svg')[0];
                    svg.style.height = scope.height ? scope.height : '500px';
                    svg.style.width = scope.width ? scope.width : '100%';

                    //Init D3
                    d3Service.get()
                        .then(function(d3) {
                            _d3 = d3;
                            //Listen to data model changes
                            scope.$on('dataModel::viewModelUpdate', function(event) {
                                //Fetch data from viewmodel
                                _vm = dataModel.getViewModel();
                                scope.nodes = _vm.nodes,
                                scope.links = _vm.links;
                                //Update D3 render model
                                drawD3Tree();
                                //Force re-render
                                scope.$apply();
                            });
                            //TODO: Remove duplicate code
                            scope.$on('dataModel::ready', function(event){
                                _vm = dataModel.getViewModel();
                                scope.nodes = _vm.nodes,
                                scope.links = _vm.links;
                                drawD3Tree();
                            });
                        });            

                    //Update visualization
                    function drawD3Tree() {
                        if(!scope.nodes || scope.nodes.length === 0){
                            return;
                        }
                        if (_d3) {
                            var tree = _d3.layout.tree();
                            tree.size([svg.offsetHeight, svg.offsetWidth-200]);
                            tree.separation(function separation(a, b) {
                                return a.parent == b.parent ? 2 : 4;
                            });
                            //Insert root node, d3 does the rest
                            tree.nodes(scope.nodes[0]);
                        }
                    }
                }
            }
    }]);
})();
